const graphql = require('graphql')
const _ = require('underscore')

function identity(value){
  return value
}

const check = (ast) => {
  if (ast.kind == 'ObjectValue' && ast.fields.length > 0) {
    return parseLiteral(ast)
  } else {
    throw new Error("Incorrect JSON data type! please refer to {aa: 11, bb: 22}")
  }
}

const parseLiteral = (ast) => {
  switch (ast.kind) {
    case 'StringValue':
      return String(ast.value)
    case 'BooleanValue':
      return Boolean(ast.value)
    case 'IntValue':
    case 'FloatValue':
      return Number(ast.value)
    case 'ObjectValue':
      {
        let obj = {}
        _.each(ast.fields, (val) => {
          obj[val.name.value] = parseLiteral(val.value)
        })
        return obj
      }
    case 'ListValue':
      return ast.values.map(parseLiteral)
    default:
      throw new Error("Incorrect JSON data type! please refer to {aa: 11, bb: 22}")
  }
}

const jsonType = new graphql.GraphQLScalarType({
  name: 'JSON',
  description: 'JSON (JavaScript Object Notation) is a lightweight data-interchange format. It is easy for humans to read and write',
  serialize: identity,
  parseValue: identity,
  parseLiteral: check
})

module.exports = jsonType
